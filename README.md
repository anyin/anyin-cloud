# Anyin 微服务脚手架

#### 微服务项目
* [anyin-center-auth 统一认证服务](https://gitee.com/anyin/anyin-center-auth)
* [anyin-center-base 基础服务](https://gitee.com/anyin/anyin-center-base)
* [anyin-center-common 公共模块](https://gitee.com/anyin/anyin-center-common)
* [anyin-center-gateway 网关服务](https://gitee.com/anyin/anyin-center-gateway)
* [anyin-center-upms 统一用户权限模块](https://gitee.com/anyin/anyin-center-upms)
* [anyin-center-cdms 统一客户管理模块](https://gitee.com/anyin/anyin-center-cdms)
* [anyin-center-websocket Websocket模块](https://gitee.com/anyin/anyin-center-websocket)

#### 前端项目
* [anyin-cloud-parent 前端基座](https://gitee.com/anyin/anyin-cloud-parent)
* [anyin-cloud-base 基础模块](https://gitee.com/anyin/anyin-cloud-base)
* [anyin-cloud-upms 统一用户权限模块](https://gitee.com/anyin/anyin-cloud-upms)

### 本机端口规划
* Nacos 8848
* Gateway 8080
* Base 8081
* Auth 8082
* Upms 8083
* Cdms 8084
* Websocket 8085

### 组件版本
* Spring Boot 2.6.1
* Spring Cloud 2021.0.0
* Spring Cloud Alibaba 2.2.6.RELEASE

### TODO LIST
- [x] 路由配置
- [ ] 字典配置
- [ ] 参数配置  
- [ ] 用户管理
- [ ] 角色管理
- [ ] 菜单管理
- [ ] 登录和权限管理

### 联系我
公众号：Anyin灬

![公众号](./02_images/anyin公众号.jpg)