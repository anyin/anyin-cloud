# Anyin Center Common 

微服务公共模块

### 基于相同网段优先的客户端负载均衡
配置：
```yaml
spring:
  cloud:
    loadbalancer:
      configurations: net-segment
```